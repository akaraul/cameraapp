//
//  PhotosViewController.swift
//  cameraApp
//
//  Created by Andrey Karaulov on 31.10.17.
//  Copyright © 2017 Andrey Karaulov. All rights reserved.
//

import UIKit

class PhotosViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITabBarControllerDelegate {
    
    @IBOutlet weak var photoTable: UITableView!
    
    var dirContents = [URL]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.delegate = self
        photoTable.register(UINib(nibName: "PhotoTableViewCell", bundle: nil), forCellReuseIdentifier: "cellPhoto")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        photoTable.reloadData()
    }
    
    func loadPhotosFromFolder(){
        let fileManager = FileManager.default
        let docsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let photoDir = docsURL.appendingPathComponent("photos")
        if !fileManager.fileExists(atPath: photoDir.path) {
            do {
                try fileManager.createDirectory(atPath: photoDir.path,
                                                withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("Error: \(error.localizedDescription)")
            }
        }
        dirContents = try! fileManager.contentsOfDirectory(at: photoDir, includingPropertiesForKeys: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        loadPhotosFromFolder()
        return dirContents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = photoTable.dequeueReusableCell(withIdentifier: "cellPhoto", for: indexPath) as! PhotoTableViewCell
        cell.cellImage.image = UIImage(contentsOfFile: dirContents[indexPath.row].path)
        cell.cellLabel.text = dirContents[indexPath.row].lastPathComponent
        return cell
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController is CameraViewController {
            tabBarController.performSegue(withIdentifier: "cameraSegue", sender: nil)
            return false
        }
        return true
    }
    
}
