//
//  CameraViewController.swift
//  cameraApp
//
//  Created by Andrey Karaulov on 01.11.17.
//  Copyright © 2017 Andrey Karaulov. All rights reserved.
//

import UIKit
import AVFoundation

class CameraViewController: UIViewController, AVCapturePhotoCaptureDelegate {

    var captureSession = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    var photoOutput: AVCapturePhotoOutput?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCaptureSession()
        setupDevice()
        setupInputOutput()
        setupPreviewlayer()
        startCaptureSession()
    }
    
    func setupCaptureSession() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }
    
    func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        for device in devices {
            if (device.position == AVCaptureDevice.Position.back){
                backCamera = device
            }
            else if (device.position == AVCaptureDevice.Position.front) {
                frontCamera = device
            }
        }
        currentCamera = backCamera
    }
    
    func setupInputOutput() {
        do {
            let captureDeviceInput = try AVCaptureDeviceInput(device: currentCamera!)
            captureSession.addInput(captureDeviceInput)
            photoOutput = AVCapturePhotoOutput()
            photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format:[AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil)
            captureSession.addOutput(photoOutput!)
         } catch {
            print(error )
        }
    }
    
    func setupPreviewlayer() {
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraPreviewLayer?.connection?.videoOrientation =  AVCaptureVideoOrientation.landscapeRight
        var previewFrame = CGRect(x: 0, y: 0, width: self.view.frame.height, height: self.view.frame.width)
        print(UIApplication.shared.statusBarOrientation)
        if UIApplication.shared.statusBarOrientation.isLandscape {
            previewFrame = view.frame
        }
        view.layer.insertSublayer(cameraPreviewLayer!, at: 0)
        cameraPreviewLayer?.frame = previewFrame
    }
    
    func startCaptureSession() {
        captureSession.startRunning()
    }
    
    func savePhoto( imageData: Data) {
        let fileManager = FileManager.default
        let docsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let newDir = docsURL.appendingPathComponent("photos")
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let fileName = formatter.string(from: date)
        let path = newDir.appendingPathComponent(fileName)
        do {
            try imageData.write(to: path, options: .atomic)
        } catch {
            print(error)
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .landscapeRight
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    @IBAction func takePhoto(_ sender: Any) {
        let settings = AVCapturePhotoSettings()
        if let photoOutputConnection = photoOutput?.connection(with: AVMediaType.video) {
            photoOutputConnection.videoOrientation = .landscapeRight
        }
        photoOutput?.capturePhoto(with: settings, delegate: self)
    }
    
    @IBAction func closeCamera(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let imageData = photo.fileDataRepresentation(){
            savePhoto(imageData: imageData)
        }
    }
}
